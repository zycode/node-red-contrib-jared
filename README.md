# node-red-contrib-jared

Jared UI nodes for Node-RED.

## Create a Jared App from Scratch

```sh
mkdir ~/jared-demo
cd ~/jared-demo
npm install --save node-red jared node-red-contrib-jared
```

## Jared Environment Variable

There is a `Layout Edit Mode` for user to design their own UI layout by dragging and dropping UI components. But this is only available in `Development` mode, because you may not want anyone can change the layout of your app. So, once you have done the app design, you should release your app with `Production` mode.

### Development Mode

```sh
cd ~/jared-demo
echo "NODE_ENV=development" > jared.env
```

### Production Mode

```sh
cd ~/jared-demo
echo "NODE_ENV=production" > jared.env
```

## Run Jared App

```sh
node node_modules/node-red/red.js -u `pwd`
```

Visit Jared App: `http://localhost:1880/jared`  
Visit Jared App Editor (Node-RED): `http://localhost:1880/red`

Enjoy!

## Add Jared to Your Existing Node-RED Flow

### Install

```sh
cd $HOME/.node-red
npm install jared node-red-contrib-jared
```

Reference: [Installing npm packaged nodes
](https://nodered.org/docs/getting-started/adding-nodes)

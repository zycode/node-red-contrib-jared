const fs = require('fs')
const path = require('path')
const mkdirp = require('mkdirp')

class PanelLayoutFileManager {
  constructor (layoutDir) {
    this.basePath = layoutDir
    mkdirp.sync(this.basePath)
  }

  read (panel) {
    return new Promise((resolve, reject) => {
      const file = path.join(this.basePath, `${panel.name}.json`)
      fs.readFile(file, (readError, data) => {
        if (!readError) {
          try {
            // data = data.toString().replace(/"static":(true|false)/g, `"static":${pinned}`);
            resolve(JSON.parse(data.toString()))
          } catch (parseError) {
            console.error(`Invalid layout data: ${data.toString()}`)
            reject(parseError)
          }
        } else {
          if (readError.code === 'ENOENT') {
            /* Check if panel uses responsive layout (routing panel) */
            if (panel.route) {
              resolve({}) /* empty responsive layouts */
            } else {
              resolve([]) /* empty layout */
            }
          } else {
            reject(readError)
          }
        }
      })
    })
  }

  write (panel, layout) {
    return new Promise((resolve, reject) => {
      const file = path.join(this.basePath, `${panel.name}.json`)
      const data = JSON.stringify(layout).replace(/"static":false/g, '"static":true')
      fs.writeFile(file, data, (error) => {
        if (error) reject(error)
        else resolve()
      })
    })
  }
}

module.exports = PanelLayoutFileManager

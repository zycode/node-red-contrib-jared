const fetch = require('node-fetch')

class UserManager {
  constructor (authServerURL) {
    this.baseURL = authServerURL
  }

  getCurrentUser ({ reqHeaders }) {
    return fetch(`${this.baseURL}/settings`, {
      method: 'get',
      headers: {
        'Authorization': reqHeaders.authorization || '',
        'Content-Type': 'application/json'
      }
    })
      .then(response => new Promise((resolve, reject) => {
        if (response.ok) resolve(response)
        else reject(response)
      }))
      .then(response => response.json())
  }

  signup ({ form }) {
    // TBD
  }

  login ({ username, password }) {
    return fetch(`${this.baseURL}/auth/token`, {
      method: 'post',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        client_id: 'node-red-admin',
        grant_type: 'password',
        scope: '*',
        username,
        password
      })
    })
      .then(response => new Promise((resolve, reject) => {
        if (response.ok) resolve(response)
        else reject(response)
      }))
      .then(response => response.json())
  }

  logout ({ reqHeaders, body }) {
    return fetch(`${this.baseURL}/auth/revoke`, {
      method: 'post',
      headers: {
        'Authorization': reqHeaders.authorization || '',
        'Content-Type': 'application/json'
      },
      body
    })
  }
}

module.exports = UserManager

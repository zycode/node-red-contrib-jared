const express = require('express')

exports.createRouter = function (controllers) {
  const router = express.Router()
  const {
    appInfoController,
    userController,
    localeFileController,
    panelLayoutFileController,
    componentController
  } = controllers

  router.get('/info', appInfoController.getInfo)
  router.get('/panels', componentController.getPanels)
  router.get('/components', componentController.getComponents)
  router.get('/i18n/:lang/:namespace', localeFileController.getTranslation)
  router.put('/i18n/:lang/:namespace', localeFileController.putTranslation)
  router.get('/layouts', componentController.middleware, panelLayoutFileController.getAllLayouts)
  router.get('/layout/:panel', componentController.middleware, panelLayoutFileController.getPanelLayout)
  router.put('/layout/:panel', componentController.middleware, panelLayoutFileController.putPanelLayout)
  router.get('/user', userController.getCurrentUser)
  router.post('/user/signup', userController.signup)
  router.post('/user/login', userController.login)
  router.post('/user/logout', userController.logout)

  return router
}

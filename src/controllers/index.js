exports.createControllers = function ({
  appInfoManager,
  userManager,
  localeFileManager,
  componentManager,
  panelLayoutFileManager
}) {
  return {
    appInfoController: require('./info')(appInfoManager),
    userController: require('./user')(userManager),
    localeFileController: require('./locale')(localeFileManager),
    componentController: require('./component')(componentManager),
    panelLayoutFileController: require('./layout')(panelLayoutFileManager)
  }
}

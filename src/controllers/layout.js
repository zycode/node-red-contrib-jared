module.exports = function (manager) {
  return {
    getAllLayouts (req, res) {
      Promise.all(req.panels.map(async (panel) => {
        const layout = await manager.read(panel)
        return { panel: panel.name, layout }
      }))
        .then((layouts) => {
          res.json(layouts)
        })
        .catch((error) => {
          res.status(500).send(error.message)
        })
    },

    async getPanelLayout (req, res) {
      if (req.panel) {
        try {
          const layout = await manager.read(req.panel)
          res.json(layout)
        } catch (error) {
          res.status(500).send(error.message)
        }
      } else {
        res.sendStatus(404)
      }
    },

    async putPanelLayout (req, res) {
      const layout = req.body.layout
      const panel = req.panel

      if (!layout) {
        res.status(400).send('No layout data')
        return
      }

      if (!panel) {
        res.status(404).send(`Given panel '${req.params.panel}' not found`)
        return
      }

      try {
        await manager.write(panel, layout)
        res.json(layout)
      } catch (error) {
        res.status(500).send(error.message)
      }
    }
  }
}

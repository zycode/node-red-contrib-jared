module.exports = function (manager) {
  return {
    async getTranslation (req, res) {
      const lang = req.params.lang
      const namespace = req.params.namespace

      if (!lang || !namespace) {
        res.status(400).send('Bad i18n parameters')
        return
      }

      try {
        const localeData = await manager.read(lang, namespace)
        res.json(localeData)
      } catch (error) {
        res.status(500).send(error.message)
      }
    },
    async putTranslation (req, res) {
      const lang = req.params.lang
      const namespace = req.params.namespace
      const data = req.body.data

      if (!lang || !namespace || !data) {
        res.status(400).send('Bad i18n parameters')
        return
      }

      try {
        await manager.write(lang, namespace, data)
        res.sendStatus(200)
      } catch (error) {
        res.status(500).send(error.message)
      }
    }
  }
}

module.exports = function (manager) {
  return {
    getInfo (req, res) {
      res.json({
        name: manager.name,
        version: manager.version,
        mode: manager.mode
      })
    }
  }
}

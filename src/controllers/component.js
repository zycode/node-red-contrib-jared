module.exports = function (manager) {
  return {
    middleware (req, res, next) {
      req.components = manager.components
      req.panels = manager.panels
      if (req.params.panel) {
        req.panel = manager.panels.find(panel => panel.name === req.params.panel)
      }
      next()
    },

    getComponents (req, res) {
      res.json(manager.components)
    },

    getPanels (req, res) {
      res.json(manager.panels)
    }
  }
}

const Jared = require('../..')

module.exports = function(RED) { // eslint-disable-line
  function Divider (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      direction: n.direction
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Divider', Divider)
}

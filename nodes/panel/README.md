# 面板 (Panel)

面板，Jared Web App最基本也最重要的UI元件，是一種用來裝載各種UI元件的容器(Container)。它允許使用者在「版面編輯模式」下對UI元件進行排版。

## 屬性 (msg.props)

| 名稱 | 型態 | 格式 | 說明 |
|-----|------|-----|-----|
| name | String | 任意文字 | 面板的名稱。 |
| route | String | 路徑格式 (/path/demo) | *`[路由面板專用]`* 面板路由路徑，若設定則表示該面板為路由面板，反之則為子面板。 |
| backgroundColor | String | 同CSS中的background-color屬性 | 面板的背景顏色。 |
| zIndex | Number | 同CSS中的z-index屬性 | *`[子面板專用]`* 設定子面板於元件重疊時的先後順序。 |
| portal | Boolean | true/false | *`[路由面板專用]`* 設定路由面板為入口面板。 |
| showBorder | Boolean | true/false | *`[子面板專用]`* 顯示子面板邊框。 |

## 資料輸入 (msg.payload)

N/A

## 命令及事件

| 代碼 | <nobr>類型</nobr> | <nobr>參數</nobr> | <nobr>回傳值</nobr> | 說明 |
|-----|------|-------------------|-------|-----|
| PANEL_ON_LOAD | 事件 | 路由參數 | N/A | 在路由面板被`載入時`會送出此事件到`Event Source` Node。 |
| PANEL_ON_UNLOAD | 事件 | 路由參數 | N/A | 在路由面板被`缷載時`會送出此事件到`Event Source` Node。 |


## 說明

面板可區分為下列三類：

1. [路由面板 (Routable Panel)](#路由面板-(routable-panel))
2. [入口面板 (Portal Panel)](#入口面板-(portal-panel))
3. [子面板 (Sub Panel)](#子面板-(sub-panel))

### 路由面板 (Routable Panel)

一個Web App可能會提供數個操作頁面，以分別對應不同的功能。在Jared中要建出這些頁面，必須透過「路由面板」來完成。假設我們有一個Web App包含了下列幾個頁面及其路由配置：

| 頁面 | 路由 | 面板名稱 |
|-----|------|---------|
| 主頁面 | / | Home Panel |
| 登入頁面 | /login | Login Panel |
| 設定主頁面 | /settings | Settings Panel |
| 系統設定頁面 | /settings/system | System Settings Panel |
| 使用者設定頁面 | /settings/users | User Settings Panel |

在Node-RED中，我們可以在Flow中依序拉出這5個路由面板：

<img src="assets/01.png" width="400px" />

其中，Home Panel的屬性設定如下：

<img src="assets/02.png" width="400px" />

在套用Flow變更後，連結到Jared App網址，我們便會看到一個背景顏色為lightskyblue的主頁面：

<img src="assets/03.png" width="600px" />

現在，你可以在網址列試著連結到其它路由面板：

- http://127.0.0.1:1880/jared/login
- http://127.0.0.1:1880/jared/settings
- http://127.0.0.1:1880/jared/settings/system
- http://127.0.0.1:1880/jared/users

以及試著連結到一個不存在的面板：

<img src="assets/04.png" width="600px" />

### 入口面板 (Portal Panel)

一般來說，一個網站的入口路徑通常都是指向路由中的根(`/`)，Jared Web App也不例外，在介紹路由面板時我們所看到的Home Panel就是一個例子，這種面板即稱為「入口面板」。

然而，在某些情況下，我們需要動態的改變Web App的入口。舉一個常見的例子，某個Web App需要使用者登入後才能進行操作，因此在尚未登入前，入口頁面會指向登入頁面(`/login`)，使用者登入後，才會連結到真正的入口面頁(`/`)。為了滿足這種需求，面板元件提供了一個 `portal` 屬性，來指定任一面板為入口面板，當然，同一時間只能有一個面板被設定成入口面板。

若要設定某個路由面板為入口面板，我們可以在Node-RED中透過該面板的屬性編輯介面，勾選 `Set as portal panel` 來設定之。但更多時候，我們需要依不同條件來動態設定它，此時便需要藉由Function Node來完成。在此我們以一個簡單的流程來模擬剛剛使用者登入的範例：

1. 首先將Login Panel預設為入口面板，並將背景顏色設定為pink：

   <img src="assets/05.png" width="400px" />

2. 以Inject Node來模擬使用者登入的動作，並加入一個Function Node來動態設定Home Panel的`portal`層性：

   <img src="assets/06.png" width="600px" />

   其中Function Node的內容為：

   <img src="assets/07.png" width="400px" />

3. 完成後，連結到主頁面可以看到入口面板預設被連結到Login Panel：

   <img src="assets/08.png" width="600px" />

4. 接著在Node-RED中按下`User logged in`來模擬使用者登入的動作，你應該會發現入口頁面自動被更新成Home Panel了：

   <img src="assets/03.png" width="600px" />

### 子面板 (Sub Panel)

一個路由面板中可能包含了大量的UI元件，在UI設計上這些元件通常會分佈在不同的群組，要建立這種群組的關係，可以透過「子面板」來達成。簡單來說，子面板就是路由面板中的面板，因為它們跟路由無關，因此不需要設定 `route` 屬性。

現在，我們就利用子面板來規畫一下Login Panel的佈局：

<img src="assets/09.png" width="600px" />

在流程中我們增加了一個Login Form Panel子面板，用來裝載登入畫面所需要的標題、輸入欄及按鈕等UI元件。Login Form Panel的屬性設定為：

<img src="assets/10.png" width="400px" />

完成流程的規劃後，我們還需要在Jared Web App中對所有UI元件進行排版，這個步驟可以透過「版面編輯模式」(`View` -> `Layout Editing Mode`) 來完成。

<img src="assets/11.png" width="600px" />

### 系統保留的路由關鍵字

系統保留了幾個路由路徑，開發人員必須實作出這些路由面板，以提供出基本的功能。

- `/`: 首頁
- `/login`: 使用者登入頁面
- `/signup`: 使用者註冊頁面
- `/profile`: 使用者資訊頁面

## 範例代碼

```json
[
    {
        "id": "7ca349b4.7dada8",
        "type": "tab",
        "label": "Flow 1",
        "disabled": false,
        "info": ""
    },
    {
        "id": "bb1665c5.78d598",
        "type": "Panel",
        "z": "7ca349b4.7dada8",
        "name": "Home Panel",
        "route": "/",
        "backgroundColor": "lightskyblue",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 820,
        "y": 180,
        "wires": [
            []
        ]
    },
    {
        "id": "3b931fde.7943f",
        "type": "Panel",
        "z": "7ca349b4.7dada8",
        "name": "Login Panel",
        "route": "/login",
        "backgroundColor": "pink",
        "zIndex": "",
        "portal": true,
        "showBorder": false,
        "x": 830,
        "y": 240,
        "wires": [
            []
        ]
    },
    {
        "id": "141b9134.cd8f3f",
        "type": "Panel",
        "z": "7ca349b4.7dada8",
        "name": "Settings Panel",
        "route": "/settings",
        "backgroundColor": "",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 850,
        "y": 300,
        "wires": [
            []
        ]
    },
    {
        "id": "e66727ec.d748b8",
        "type": "Panel",
        "z": "7ca349b4.7dada8",
        "name": "System Settings Panel",
        "route": "/settings/system",
        "backgroundColor": "",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 900,
        "y": 360,
        "wires": [
            []
        ]
    },
    {
        "id": "7aa1cae7.412c34",
        "type": "Panel",
        "z": "7ca349b4.7dada8",
        "name": "User Settings Panel",
        "route": "/settings/users",
        "backgroundColor": "",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 890,
        "y": 420,
        "wires": [
            []
        ]
    },
    {
        "id": "d4cf5486.bbd758",
        "type": "Button",
        "z": "7ca349b4.7dada8",
        "name": "Login Button",
        "text": "Login",
        "size": "big",
        "color": "green",
        "icon": "",
        "basic": false,
        "circular": false,
        "disabled": false,
        "loadingOnClick": false,
        "action": "ui data link",
        "x": 390,
        "y": 300,
        "wires": [
            [
                "6912e2b2.965cdc"
            ]
        ]
    },
    {
        "id": "6912e2b2.965cdc",
        "type": "Panel",
        "z": "7ca349b4.7dada8",
        "name": "Login Form Panel",
        "route": "",
        "backgroundColor": "rgba(200,200,200,.6)",
        "zIndex": "",
        "portal": false,
        "showBorder": false,
        "x": 610,
        "y": 260,
        "wires": [
            [
                "3b931fde.7943f"
            ]
        ]
    },
    {
        "id": "946c6b69.e302e8",
        "type": "inject",
        "z": "7ca349b4.7dada8",
        "name": "User logged in",
        "topic": "",
        "payload": "true",
        "payloadType": "bool",
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "x": 520,
        "y": 120,
        "wires": [
            [
                "8bac3f4a.6c526"
            ]
        ]
    },
    {
        "id": "8bac3f4a.6c526",
        "type": "function",
        "z": "7ca349b4.7dada8",
        "name": "Set as portal",
        "func": "return {\n    props: {\n        portal: true\n    }\n}",
        "outputs": 1,
        "noerr": 0,
        "x": 630,
        "y": 180,
        "wires": [
            [
                "bb1665c5.78d598"
            ]
        ]
    },
    {
        "id": "8a0df50a.da9048",
        "type": "Input",
        "z": "7ca349b4.7dada8",
        "name": "User Input",
        "defaultValue": "",
        "placeholder": "E-Mail",
        "size": "large",
        "textAlign": "right",
        "inputType": "text",
        "min": "",
        "max": "",
        "step": "",
        "icon": "user",
        "iconPosition": "right",
        "disabled": false,
        "x": 390,
        "y": 220,
        "wires": [
            [
                "6912e2b2.965cdc"
            ]
        ]
    },
    {
        "id": "a0644f06.ed6f9",
        "type": "Input",
        "z": "7ca349b4.7dada8",
        "name": "Password Input",
        "defaultValue": "",
        "placeholder": "Password",
        "size": "large",
        "textAlign": "right",
        "inputType": "password",
        "min": "",
        "max": "",
        "step": "",
        "icon": "lock",
        "iconPosition": "right",
        "disabled": false,
        "x": 380,
        "y": 260,
        "wires": [
            [
                "6912e2b2.965cdc"
            ]
        ]
    },
    {
        "id": "5161c9b9.8af318",
        "type": "Space",
        "z": "7ca349b4.7dada8",
        "name": "",
        "x": 380,
        "y": 340,
        "wires": [
            [
                "6912e2b2.965cdc"
            ]
        ]
    },
    {
        "id": "5cd72929.741fa8",
        "type": "Space",
        "z": "7ca349b4.7dada8",
        "name": "",
        "x": 620,
        "y": 220,
        "wires": [
            [
                "3b931fde.7943f"
            ]
        ]
    },
    {
        "id": "e05f0e3a.cc412",
        "type": "Header",
        "z": "7ca349b4.7dada8",
        "name": "Welcome Header",
        "text": "Welcome to Jared Dashboard",
        "size": "huge",
        "color": "",
        "textAlign": "center",
        "icon": "dashboard",
        "dividing": false,
        "disabled": false,
        "x": 370,
        "y": 180,
        "wires": [
            [
                "6912e2b2.965cdc"
            ]
        ]
    }
]
```

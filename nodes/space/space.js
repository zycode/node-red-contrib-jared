const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Space (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Space', Space)
}

/* TODO: make this node outside jared */

function generateWaveform (type, length, offset, header) {
  let YBase = 0
  let amplitude = [-30, 30]
  let pulseWidth = 100
  let resolution = 1
  let waveformData = []

  for (let x = offset; x < length + offset; x += resolution) {
    if (type === 'square') {
      let peakIndex = parseInt((x % pulseWidth * 2) / pulseWidth, 10)
      waveformData.push(amplitude[peakIndex] + YBase)
    }
  }

  return {
    header,
    rawdata: waveformData
  }
}

// eslint-disable-next-line
module.exports = function(RED) {
  function SignalGenerator (n) {
    RED.nodes.createNode(this, n)

    const sigHeader = {
      vScale: Number(n.sighdr_vscale),
      sampleRate: Number(n.sighdr_samplerate),
      startTime: Number(n.sighdr_starttime),
      offset: 0
    }

    this.on('input', (msg) => {
      this.send({
        payload: generateWaveform(
          n.sigtype,
          Number(n.siglen),
          Number(n.sigoffs),
          sigHeader
        )
      })
    })

    this.on('close', (removed, done) => {
      if (removed) {
        // This node has been deleted
      } else {
        // This node is being restarted
        // TODO: should notify jared ui changes
      }
      done()
    })
  }

  RED.nodes.registerType('Signal Generator', SignalGenerator)
}

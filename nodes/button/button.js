const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Button (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      text: n.text,
      size: n.size,
      color: n.color,
      icon: n.icon,
      image: n.image,
      basic: n.basic,
      circular: n.circular,
      disabled: n.disabled,
      loadingOnClick: n.loadingOnClick,
      action: n.action,
      actionPayload: n.actionPayload
    }

    jared.addComponent(this, {
      nodeEventHandlers: { BUTTON_ON_CLICK: null }, /* Use default event handler */
      nodeInputHandler: (msg) => {
        if (msg.payload !== undefined) {
          if (msg.component.type === 'Input') {
            if (msg.component.name.toLowerCase().includes('username')) {
              this.rtc.broadcast(this, {
                dataType: 'data',
                payload: { username: msg.payload },
                index: msg.index
              })
            }

            if (msg.component.name.toLowerCase().includes('password')) {
              this.rtc.broadcast(this, {
                dataType: 'data',
                payload: { password: msg.payload },
                index: msg.index
              })
            }
          } else {
            this.rtc.broadcast(this, {
              dataType: 'data',
              payload: msg.payload,
              index: msg.index
            })
          }
        }

        if (msg.props) {
          this.props = {
            ...this.props,
            ...msg.props,
            // set below to prevent id and jcid being overrided
            id: this.props.id,
            jcid: this.props.jcid
          }
          this.rtc.broadcast(this, {
            dataType: 'props',
            payload: msg.props,
            index: msg.index
          })
        }
      }
    })
  }

  RED.nodes.registerType('Button', Button)
}

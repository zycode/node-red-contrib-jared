const Jared = require('../..')

// eslint-disable-next-line
module.exports = function(RED) {
  function Image (n) {
    RED.nodes.createNode(this, n)

    const jared = Jared(RED)

    this.props = {
      id: n.id,
      jcid: n.jcid,
      type: n.type,
      name: n.name,
      source: n.source
    }

    jared.addComponent(this)
  }

  RED.nodes.registerType('Image', Image)
}
